<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainM extends CI_Model
{
    public function updloadImage($name, $path)
    {
        $config['upload_path']    = $path;
        $config['allowed_types']  = "jpeg|jpg|png|ico";
        $config['remove_spaces']  = true;
        $config['file_ext_tolower']  = true;
        $config['file_name']  = date('d-m-Y')."-".$_FILES[$name]["name"];

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload($name)) {
            return  array(
                'status'  =>  400,
                'data'    =>  '',
                'error'   =>  $this->upload->display_errors()
            );
        }

        return  array(
            'status'  =>  200,
            'data'    =>  $this->upload->data(),
            'error'   =>  ''
        );
    }
}