<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadController extends CI_Controller
{

    protected $path = './assets/img/config/';
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MainM', 'mainm');
    }
    
    public function store()
    {
        $data = $this->input->post();
        $data = $this->security->xss_clean($data);

        if ($_FILES['logo']['name'] != '') {
            if ( ! is_dir($this->path)) {
                if ( ! mkdir($this->path, 0777, true)) {
                    exit;
                }
            }

            $doUpload = $this->mainm->updloadImage('logo', $this->path);
            if ($doUpload['status'] == 200) {
                $return['data'] = $doUpload['data'];
            }
        }

        echo json_encode($return);
        exit;
    }
}